EAPI=4

DESCRIPTION="Virtual package for devel tools and programming languages"
HOMEPAGE="http://pernatiy.mooo.com/"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~sparc x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~x64-macos ~x86-solaris"
IUSE="
+clang
d
erlang
lisp
+perl
prolog
+tools
visual
"

RDEPEND="
	clang? (
		sys-devel/clang
	)
	d? (
		dev-lang/dmd
	)
	erlang? (
		dev-lang/erlang
	)
	lisp? (
		dev-lisp/clisp
	)
	perl? (
		dev-lang/perl
	)
	prolog? (
		dev-lang/gprolog
	)

	tools? (
		dev-util/strace
		dev-util/valgrind
		dev-vcs/git
		sys-devel/crossdev
		|| (
			<sys-devel/gdb-7.12[expat]
			>=sys-devel/gdb-7.12[xml]
		)
		visual? (
			kde-apps/kde-apps-meta[sdk]
			media-gfx/plantuml
		)
	)
"

DEPEND=""
