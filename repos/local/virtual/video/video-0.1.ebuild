EAPI=4

DESCRIPTION="Virtual package for video tools"
HOMEPAGE="http://pernatiy.mooo.com/"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~sparc x86 ~amd64-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~x64-macos ~x86-solaris"
IUSE="+edit"

RDEPEND="
	media-video/mplayer
	media-video/mpv

	edit? (
		media-video/cinelerra
		media-video/lives
		media-video/openshot
		media-video/pitivi
		kde-apps/kdenlive
	)
"

DEPEND=""
