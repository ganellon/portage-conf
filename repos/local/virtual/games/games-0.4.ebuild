EAPI=4

DESCRIPTION="Virtual package for prefered games"
HOMEPAGE="http://pernatiy.mooo.com/"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 mips ppc ppc64 sparc x86 amd64-fbsd x86-fbsd amd64-linux x86-linux x64-macos x86-solaris"
IUSE="+action arcade rpg +fps strategy big input_devices_joystick"

RDEPEND="
	action? (
		games-action/abuse
		games-action/atanks
		games-action/teeworlds
	)
	arcade? (
		games-arcade/opensonic
		games-arcade/tuxdash
	)
	rpg? (
		games-engines/gemrb
	)
	strategy? (
		games-strategy/freeciv
		games-strategy/freeorion
		games-strategy/maxr
		big? (
			games-strategy/ufo-ai
		)
	)
	fps? (
		media-libs/alsa-oss
		games-util/xqf
		games-fps/quake3-bin
		big? (
			games-fps/nexuiz
			games-fps/warsow
			games-fps/xonotic
		)
	)
	input_devices_joystick? (
		games-util/joystick
		games-util/xboxdrv
		games-util/ds4drv
	)
"

DEPEND=""
