# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=(python{2_7,3_3,3_4})
DISTUTILS_SINGLE_IMPL=true
inherit distutils-r1 eutils

DESCRIPTION="Userspace Dualshock 4 driver"
HOMEPAGE="https://github.com/chrippa/ds4drv"
SRC_URI="https://github.com/chrippa/ds4drv/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~hppa ~ppc ~ppc64 ~x86 ~amd64-linux ~arm-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~x86-solaris"
IUSE=""

DEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	>=dev-python/pyudev-0.16
	>=dev-python/python-evdev-0.3.0
"
